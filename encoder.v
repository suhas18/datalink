
/*Takes 9 bit input and encodes it into a 10 bit balanced output. Of the 9 bits i/p first bit 
* is data_notCtrl which decides if input is data or control code. With data_notCtrl = 1,
* there are 256 data codes - 248 balanced and 8 unbalanced; with data_notCtrl = 0, there are 64 control codes - unbalanced.
* Encoding done by combination of 5 bit words - (3,2),(2,3),(4,1) & (1,4).
* Encoding functions are implemented as modules and are concatinated according to input patterns.  
* Non encodable input - non control codes when data_notCtrl = 0, asserts an error output.
*/

`timescale 1ns / 1ps

module encoder (
                       input [7:0] data_i
                      ,input data_notCtrl_i
                      ,output [9:0] out10b_o
                      ,output error_flag_o
                    );

//  reg [4:0] h3_msb;
//  reg [4:0] h3_lsb;
//  reg [4:0] h2_msb;
//  reg [4:0] h2_lsb;
//  reg [4:0] h4_msb;
//  reg [4:0] h4_lsb;
//  reg [4:0] h1_msb;
//  reg [4:0] h1_lsb;

  logic [4:0] h3_msb;
  logic [4:0] h3_lsb;
  logic [4:0] h2_msb;
  logic [4:0] h2_lsb;
  logic [4:0] h4_msb;
  logic [4:0] h4_lsb;
  logic [4:0] h1_msb;
  logic [4:0] h1_lsb;
        
  logic [9:0] out10b;
  logic error_flag_o;
  
  h3_encode h3_upper ( .i(data_i[5:3])
                      ,.o(h3_msb)
                     );
  h3_encode h3_lower ( .i(data_i[2:0])
                      ,.o(h3_lsb)    
                     );
  h2_encode h2_upper ( .i(data_i[5:3])
                      ,.o(h2_msb)
                     );
  h2_encode h2_lower ( .i(data_i[2:0])
                      ,.o(h2_lsb)
                     );
    
  h4_encode h4_upper ( .i(data_i[3:2])
                      ,.o(h4_msb)
                     );
  h4_encode h4_lower ( .i(data_i[1:0])
                      ,.o(h4_lsb)    
                     );
  h1_encode h1_upper ( .i(data_i[3:2])
                      ,.o(h1_msb)    
                     );
  h1_encode h1_lower ( .i(data_i[1:0])
                      ,.o(h1_lsb)    
                     );
                        
  always_comb
  begin
    casez({data_notCtrl_i, data_i})
      //TODO handle all data_notCtrl_i=0 cases
            9'b0_00_??????:  begin
                                out10b = {h3_msb,h3_lsb};
                                error_flag_o = 0;
                             end //64 unbalanced control cases
            9'b0_1000_0000: begin
                                out10b = {5'b10100,5'b11010}; 
                                error_flag_o = 0;           
                            end //no data case, balanced
            9'b1_00_?????? : begin
                                out10b = {h3_msb,h2_lsb};
                                error_flag_o = 0;   
                             end //64 cases
            9'b1_01_?????? : begin
                                out10b = {h2_msb,h3_lsb};
                                error_flag_o = 0;   
                             end //64 cases
            9'b1_10_00_???? : begin
                                if(data_i[3])
                                    out10b = {5'b11100,h2_lsb};
                                    
                                else
                                    out10b = {5'b11010,h2_lsb};
                              error_flag_o = 0;   
                              end //16 cases
            9'b1_10_01_???? : begin
                                if(data_i[3])
                                    out10b = {5'b11000,h3_lsb};
                                else
                                    out10b = {5'b10100,h3_lsb};
                              error_flag_o = 0;   
                              end //16 cases
            9'b1_10_10_???? : begin
                                if(data_i[3])
                                    out10b = {h3_lsb,5'b11000};
                                else
                                    out10b = {h3_lsb,5'b10100};
                              error_flag_o = 0;   
                              end //16 cases
            9'b1_10_11_???? : begin
                                if(data_i[3])
                                    out10b = {h2_lsb,5'b11100};
                                else
                                    out10b = {h2_lsb,5'b11010};
                              error_flag_o = 0;   
                              end //16 cases
            9'b1_11_00_???? : begin
                                out10b = {h4_msb,h1_lsb};
                                error_flag_o = 0;   
                              end //16 cases
            9'b1_11_01_???? : begin
                                out10b = {h1_msb,h4_lsb};
                                error_flag_o = 0;   
                              end //16 cases
            9'b1_11_10_00_?? : begin
                                out10b = {5'b01111,h1_lsb};
                                error_flag_o = 0;   
                               end //4 cases
            9'b1_11_10_01_?? : begin
                                out10b = {5'b10000,h4_lsb};
                                error_flag_o = 0;   
                               end //4 cases
            9'b1_11_10_10_?? : begin
                                out10b = {h4_lsb,5'b10000};
                                error_flag_o = 0;   
                               end //4 cases
            9'b1_11_10_11_?? : begin
                                out10b = {h1_lsb,5'b01111};
                                error_flag_o = 0;   
                               end //4 cases
            9'b1_11_11_00_?? : begin
                                if(data_i[1])
                                begin
                                    if(data_i[0])
                                        out10b={5'b11100,5'b11000}; //{h3(9),h2(9)}
                                    else
                                        out10b={5'b11100,5'b10100};//{h3(9),h2(8)};
                                end
                                else
                                begin
                                    if(data_i[0])
                                        out10b={5'b11010,5'b11000};//{h3(8),h2(9)};
                                    else
                                        out10b={5'b11010,5'b10100};//{h3(8),h2(8)};
                                end
                               error_flag_o = 0;   
                               end //4 cases
            9'b1_11_11_01_0?: begin
                                if(data_i[0])
                                    out10b = {5'b11000,5'b11100};//{h2(9),h3(9)}
                                else
                                    out10b = {5'b11000,5'b11010}; //{h2(9),h3(8)}
                               error_flag_o = 0;   
                               end //2 cases
            //individual cases                  
            9'b1_11_11_01_10: begin
                                out10b = {5'b01111,5'b10000}; //{h4(4),h1(4)}
                                error_flag_o = 0;   
                              end //1 case
            9'b1_11_11_01_11: begin
                                out10b = {5'b10000,5'b01111}; //{h1(4),h4(4)}
                                error_flag_o = 0;   
                               end //1 case
            
            //8 unbalanced cases
            9'b1_11_11_1_??? : begin
                                out10b = {5'b11010,h3_lsb};
                                error_flag_o = 0;   
                               end //8 unbalanced cases
            default:          begin
                                error_flag_o = 1; 
                                out10b = 0; //What should output be when encoder raises an error  
                                $display("Error in encoder. 8 bit input not mapped");
                              end
            
            endcase
    
    end //always block               
    assign out10b_o = out10b;            
endmodule

//modules which take 3 bit input and encode them into 5 bit groups which contribute to balanced codes
module h3_encode (
                    input  [2:0] i
                   ,output [4:0] o
                 );
  logic [4:0] out;
  always_comb
  begin
    case(i)
      0: out = 5'b00111;
      1: out = 5'b01011;
      2: out = 5'b01101;
      3: out = 5'b01110;
      4: out = 5'b10011;
      5: out = 5'b10101;
      6: out = 5'b10110;
      7: out = 5'b11001;
      8: out = 5'b11010;
      9: out = 5'b11100;
      default: out = 5'b?; //what should the default output be?
    endcase  
  end //end always
  assign o = out;                   
                    
endmodule

module h2_encode (
                     input  [2:0] i
                    ,output [4:0] o
                 );
  logic [4:0] out;
  always_comb
  begin
    case(i)
      0: out = 5'b00011;
      1: out = 5'b00101;
      2: out = 5'b00110;
      3: out = 5'b01010;
      4: out = 5'b01100;
      5: out = 5'b01001;
      6: out = 5'b10001;
      7: out = 5'b10010;
      8: out = 5'b10100;
      9: out = 5'b11000;
      default: out = 5'b?;
    endcase  
  end //end always
  assign o = out;                   
                    
endmodule   
        
module h4_encode (
                     input  [1:0] i
                    ,output [4:0] o
                 );
  logic [4:0] out;
  always_comb
  begin
    case(i)
      0: out = 5'b11110;
      1: out = 5'b11101;
      2: out = 5'b11011;
      3: out = 5'b10111;
      4: out = 5'b01111;
      default: out = 5'b?;
    endcase  
  end //end always
  assign o = out;                   
                    
endmodule 

module h1_encode (
                     input  [1:0] i
                    ,output [4:0] o
                 );
  logic [4:0] out;
  always_comb
  begin
    case(i)
      0: out = 5'b00001;
      1: out = 5'b00010;
      2: out = 5'b00100;
      3: out = 5'b01000;
      4: out = 5'b10000;
      default: out = 5'b?;
    endcase  
  end //end always
  assign o = out;                   
                    
endmodule 
