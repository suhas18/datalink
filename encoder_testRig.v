//This module instantiates the encoder and places components around it for testing on the board.
//A multiplexer selects one 8bit word from either of 2 counters. The counters 
//Trying with 16 bit counters. Two 8 bit words can be separately handled.
`include "encoder.v"
`define SEND_CLK 5
`define WIDTH 16


module encoder_testRig (
                           input clk_i
                          ,input reset_i
                          ,output [9:0] rig_o
                          ,output clk_o
                       );   

  localparam width_lp = `WIDTH;
  
  // logic reset_i; //reset controlled from a button on sending FPGA
  
  //TODO: reset initializes state to a sync state which pulls data_notCtrl to 0. Decoder responds to the control code by reseting its counters
  
  //data_notCtrl can be controlled to sync decoder top module
  //logic data_notCtrl = 1;
  
  logic [7:0] encoder_in_r;
  logic [9:0] encoder_out;
  logic [9:0] encoder_out_r;
  logic encoder_error;
  
  logic [7:0] mux_out;
  //TODO: sync resets
  //logic reset = 0; //there should be a FSM logic to control reset sync of encoder and decoder modules
  logic [width_lp-1:0] counter_out;
  logic half_clk;
  
  
  bsg_cycle_counter #( .width_p(`WIDTH)
                      ,.init_val_p(1'b0)
                     ) counter
                     ( .clk(half_clk) //divide clock by 2 for the counter. clk_i/2 is input
                      ,.reset_i(reset_i)
                      ,.ctr_r_o(counter_out)
                     ); 
  
  encoder enc0 (   .data_i(encoder_in_r),
                 //.data_notCtrl_i(data_notCtrl),
                   .data_notCtrl_i(1'b1),
                   .out10b_o(encoder_out),
                   .error_flag_o(encoder_error) 
               );
  
  bsg_cycle_counter #( .width_p(1) //one bit width. For clock division by 2
                      ,.init_val_p(1'b0)
                     ) one_bit_counter
                     ( .clk(clk_i)
                      ,.reset_i(reset_i)
                      ,.ctr_r_o(half_clk)
                     );
                     
  assign rig_o = encoder_out_r;
  
  //This clock is not clk_i. Should be a generated clock on whose both edges decoder operates. 
  assign clk_o = half_clk;
  
  //in positive cycles of clock, choose lower half of counter
  //in negative cycles of clock, choose upper half of counter
  
  assign mux_out = half_clk
                  ? counter_out[(`WIDTH/2)-1:0]
                  : counter_out[(`WIDTH)-1:(`WIDTH/2)];
/*  if(clk_i) //ternary operator can be used
    assign mux_out = counter_out[(`WIDTH/2)-1:0];
  else
    assign mux_out = counter_out[(`WIDTH)-1:(`WIDTH/2)];
*/
    
  //input to encoder registered to ensure simultaneous availability of all bits
  always_ff @(posedge clk_i)
  begin
    encoder_in_r <= mux_out;
    encoder_out_r <= encoder_out;
  end //always_ff
             
             
endmodule //encoder_testRig

//Counter module need not be defined here. Can be included later.
module bsg_cycle_counter #(parameter width_p=32
                           , init_val_p = 0)
   (input clk
    , input reset_i
    , output logic [width_p-1:0] ctr_r_o);

   always @(posedge clk or reset_i) //modified from original. Didn't have async rest.
     if (reset_i)
       ctr_r_o <= init_val_p;
     else
       ctr_r_o <= ctr_r_o+1;

endmodule // bsg_cycle_counter


