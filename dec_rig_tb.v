`include "encoder.v"

`define PERIOD 10
module decoder_testRig_tb;

  
  logic clk;
  logic LED_out;
  logic [9:0] encoder_out;
  
  logic data_notCtrl;
  
  logic [7:0] data_i;
  logic [7:0] count_i, count_j;
  logic encoder_error;
  
  initial
  begin
    $dumpfile("dump.vcd");
    $dumpvars(0);
    
    clk=0;
    data_i =0;
    count_i = 0;
    count_j = 0;
    data_notCtrl = 1;
    #`PERIOD data_notCtrl = 0;
    data_i = 8'b0011_1111;
    
    #((`PERIOD * 5) + 2) data_i = 0;
      data_notCtrl = 1;
    
        
    while(count_i <= 8'd10)
    begin
      while(count_j < 8'd255)
      begin
        #(`PERIOD/2 ) data_i = count_i;
        #(`PERIOD/2 ) data_i = count_j;
        count_j = count_j + 1;
      end //inner loop
      count_i = count_i +1;
    end
    
    #(`PERIOD) $finish;
    
    
  end
  
  encoder enc  (   .data_i(data_i),
                   .data_notCtrl_i(data_notCtrl),
                 //.data_notCtrl_i(1'b1),
                   .out10b_o(encoder_out),
                   .error_flag_o(encoder_error) 
               );
  
  decoder_testRig DUT  (
                            .data_i(encoder_out)
                           ,.clk_i(clk)
                           ,.LED_o(LED_out)
                       );
                       
  always
    #(`PERIOD/2) clk = ~clk;

endmodule