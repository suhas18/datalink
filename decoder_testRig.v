//receives 10b encoded data and decodes into 8b data at every edge of the clock
//TODO: recognize a control code supplied by a FSM to reset counters

`include "decoder.v"
`define COUNTER_WIDTH_P 16

module decoder_testRig (
                         input [9:0] data_i
                        ,input clk_i
                        ,output LED_o //counter value and decoded values match when 1
                       );
                       
  localparam counter_width_lp = `COUNTER_WIDTH_P;
  
  logic reset; //reset of counter. Has to be activated from a FSM 
    
  logic [counter_width_lp-1:0] counter_out;
  
  logic [9:0] data_0_r;
  logic [9:0] data_1_r;
  
  logic [7:0] data_0_n; //output of decoder0
  logic [7:0] data_1_n; //output of decoder1
  
  logic [7:0] data_0_o_r; //dec0 op registered
  logic [7:0] data_1_o_r; //dec1 op registered
  logic       data_notCtrl_0_o_r;
  
  logic dec0_error;
  logic dec1_error;
  logic data_notCtrl_0_n;
  logic data_notCtrl_1_n;
  
  logic comp_o_0;
  logic comp_o_1;

  //decoder operating in neg edge
  decoder dec0 (.data_i(data_0_r),
                .out8b_o(data_0_n),
                .error_flag(dec0_error),
                .data_notCtrl_o(data_notCtrl_0_n)
               ); 
               
  //decoder operating in pos edge           
  decoder dec1 (.data_i(data_1_r),
                .out8b_o(data_1_n),
                .error_flag(dec1_error),
                .data_notCtrl_o(data_notCtrl_1_n)
               );
  
  //reset_control with a seq module. Not a state machine
  reset_control  #( .rst_count(10) ) rst_ctr
                  ( //.data_i(data_0_o_r)
                    .data_notCtrl(data_notCtrl_0_o_r) //Reset being done on a falling edge. Try with rise edge.
                   ,.clk_i(clk_i)
                   ,.reset_o(reset)
                  );             
  bsg_cycle_counter #( .width_p(counter_width_lp)
                      ,.init_val_p(1'b0)
                     ) counter
                     ( .clk(clk_i) 
                      ,.reset_i(reset) //TODO: figure out reset logic for sync
                      ,.ctr_r_o(counter_out)
                     );
  
  always_ff @(negedge clk_i)
  begin
    data_0_r <= data_i;
    data_0_o_r <= data_0_n;
    
    data_notCtrl_0_o_r <= data_notCtrl_0_n;
  end
  
  always_ff @(posedge clk_i)
  begin
    data_1_r <= data_i;
    data_1_o_r <= data_1_n;
  end                                       
  
  assign comp_o_0 = (data_0_o_r == counter_out[(counter_width_lp)-1:(counter_width_lp/2)])
                    ? 1'b1
                    : 1'b0; 
  
  assign comp_o_1 = (data_1_o_r == counter_out[(counter_width_lp/2)-1:0])
                    ? 1'b1
                    : 1'b0;
  
  assign LED_o = (comp_o_0 & comp_o_1); //should implement if LED_o == 0 then stop counter 
                    
endmodule //decoder_testRig                    
                       
module reset_control #(parameter rst_count = 10) //Regulates starting of receiver counters  
                     
                      (
                       //input [7:0] data_i //No need to check particular control word. If Ctrl or not is enough
                         input       data_notCtrl
                        ,input       clk_i
                        ,output      reset_o
                      );
  logic [3:0] count;
  logic reset_o;
  
  assign reset_o = ~data_notCtrl;



/*  
  always_ff @(negedge clk_i)
  begin
    count = 0;
    if (data_notCtrl == 0) //should only count reset signals when control signals are being sent
    begin
      if(data_i == 8'b0011_1111) //NOT necessary. Just data_no = 0 will do.this control code will be sent by transmitter to time reset of counter. 
        count <= count + 1;
      else
        count <= count; //Currently not a state machine. Count will still increment if the control code comes later.
      
    end //if data_notCtrl block
    
//    if (count == rst_count) 
    if (count == 1) //only to try if the counter is working
      reset_o <= 1;
    else 
      reset_o <= 0;
      
  end//always
*/

endmodule


//Counter module need not be defined here. Can be included later.
module bsg_cycle_counter #(parameter width_p=32
                           , init_val_p = 0)
   (input clk
    , input reset_i
    , output logic [width_p-1:0] ctr_r_o);

   always @(posedge clk)
     if (reset_i)
       ctr_r_o <= init_val_p;
     else
       ctr_r_o <= ctr_r_o+1;

endmodule //bsg_cycle_counter


module bsg_wait_after_reset  #(parameter lg_wait_cycles_p="inv") //Modified to go 1->0 after reset
   (input reset_i
    , input clk_i
    , output reg ready_r_o);

   logic [lg_wait_cycles_p-1:0] counter_r;

   always @(posedge clk_i)
     begin
        if (reset_i)
          begin
             counter_r <= 1;
             ready_r_o <= 1;
          end
        else
          if (counter_r == 0)
            ready_r_o <= 0;
          else
            counter_r <= counter_r + 1;
     end
endmodule //bsg_wait_after_reset