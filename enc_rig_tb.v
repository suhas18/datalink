`define PERIOD 10
module encoder_testRig_tb;

  
  logic clk;
  logic reset;
  logic [9:0] out;
  logic source_sync_clk;
  
  initial
  begin
    $dumpfile("dump.vcd");
    $dumpvars(0);
    
    clk=0;
    reset = 1;
    #(`PERIOD + 5) reset = 0;
    
    #(`PERIOD *256 *256) $finish;
  end
  encoder_testRig DUT  (
                           .clk_i(clk)
                          ,.reset_i(reset)
                          ,.rig_o(out)
                          ,.clk_o(source_sync_clk)
                       );
                       
  always
    #(`PERIOD) clk = ~clk;
  
     
endmodule