`timescale 1ns / 1ps
`define TIME_PERIOD 10
module combined_tb;

reg [7:0] data_i;
reg data_notCtrl_i;

wire data_notCtrl_o;
wire encoder_error_o;
wire decoder_error_o;
wire [9:0] encoder_10b_o;
wire [7:0] decoder_8b_o;

encoder_8b10b enc0 (   .data_i(data_i),
                       .data_notCtrl_i(data_notCtrl_i),
                       .out10b_o(encoder_10b_o),
                       .error_flag_o(encoder_error_o) 
                   );
                    
decoder dec0 (.data_i(encoder_10b_o),
              .out8b_o(decoder_8b_o),
              .error_flag(decoder_error_o),
              .data_notCtrl_o(data_notCtrl_o)
             );    

                     
    initial
      begin
    	$dumpfile("result.vcd");
    	$dumpvars(0);
        data_i = 0;
        data_notCtrl_i = 0;
        
        $monitor("output for input %b is %b",data_i, decoder_8b_o);  
     
      end
      
      always
      begin
        while(data_i < 255)
      		#`TIME_PERIOD data_i = data_i + 1; //testing all control codes
      	#`TIME_PERIOD data_notCtrl_i = 1; //asserting this signal to say input from now is data and not control
        data_i = 0;
        while(data_i < 255)
      		#`TIME_PERIOD data_i = data_i + 1; //testing all data codes
      	
      end
      
      initial
      	#(`TIME_PERIOD * 255 *2) $finish;
          
endmodule